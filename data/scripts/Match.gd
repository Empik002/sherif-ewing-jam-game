extends KinematicBody2D

onready var Fire = preload("res://data/scenes/Fire.tscn")
var velocity = Vector2.ZERO
const GRAVITY = 3
const BLAST_RADIUS = 10

func _physics_process(delta):
    velocity.y += GRAVITY
    velocity = move_and_slide(velocity)
    if get_slide_count() > 0:
        velocity.x = 0
        ignite()
        
        
func init(vel):
    velocity = vel
    return self
    

func ignite():
    var this_pos = get_global_position()
    var new_fire = Fire.instance().init(this_pos)
    Global.MainNode.add_child(new_fire)
    queue_free()
