extends Node2D

var die_sound = preload("res://data/sfx/die.wav")

func object_entered(body):
    if body.is_in_group("PlayerBullet"):
        get_parent().remove_child(self)
        queue_free()
        body.queue_free()
        AudioManager.play_sfx(die_sound)

