extends "res://data/scripts/Enemy.gd"

const OFFSET_LD = Vector2(-4,0)
const OFFSET_RD = Vector2(6,0)
const OFFSET_LU = Vector2(-5,-3)
const OFFSET_RU = Vector2(7,-3)

func _ready():
    SHOT_TIME = 1.2
    VISION = 45
    STOPDISTANCE = 30

func shoot():
    var ofstU
    var ofstD
    var direction
    
    if not anim.flip_h:
        ofstU = OFFSET_LU
        ofstD = OFFSET_LD        
        direction = -1
    else:
        ofstU = OFFSET_RU
        ofstD = OFFSET_RD        
        direction = 1
    
    Global.shoot(get_global_position(), ofstU, direction, false)
    Global.shoot(get_global_position(), ofstD, direction, false)    
