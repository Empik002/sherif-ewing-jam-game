extends AnimatedSprite

var win_sound = preload("res://data/sfx/win.wav")
func _ready():
    frame = 0
    playing = true
func _process(delta):
    if frame == 12:
        AudioManager.play_sfx(win_sound)
func ended():
    get_tree().change_scene("res://data/scenes/MainView.tscn")
