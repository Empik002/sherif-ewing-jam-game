extends Node2D

var hp = 5
const THROW_INTERVAL = 3
var throw_time = THROW_INTERVAL
const vision = 90

var die_sound = preload("res://data/sfx/die.wav")
onready var Throwable = preload("res://data/scenes/Dynamite.tscn")

func _ready():
    pass

func _process(delta):
    if Global.distance(Global.Player.get_global_position(), 
        get_global_position()) <= vision:
        
        throw_time -= delta
        if throw_time <= 0:
            throw()
            
func hurt(body): # play some sound and flicker
    if not body.is_in_group("PlayerBullet"):
        return
    hp -= 1
    body.queue_free()
    AudioManager.play_sfx(die_sound)
    if hp <= 0:
        queue_free()

func throw():
    throw_time = THROW_INTERVAL
    var dyn_pos = Vector2(get_global_position().x - 6, get_global_position().y)
    var dyn_vel = Vector2(rand_range(-50,-10), rand_range(-150,-40))
    var new_dynamite = Throwable.instance().init(dyn_vel)
    new_dynamite.set_global_position(dyn_pos)
    Global.MainNode.add_child(new_dynamite)
    
    
