extends Node2D

var music:AudioStream = preload("res://data/music/tune.wav")


func _ready() -> void:
    yield(get_tree(),"idle_frame")
    Global.SwitchColors = false
    for child in Global.white_platforms.get_children():
        child.visible = false
        child.collider.set_deferred("disabled", true)
    #AudioManager.play_music(music)
    pass

func _process(delta):
    pass
    
