extends Node

onready var Bullet = preload("res://data/scenes/Bullet.tscn")
var MainNode = null
var Player = null
var SwitchColors = false
var white_platforms = null



func _process(delta):
    if Input.is_action_just_pressed("esc"):
        get_tree().change_scene("res://data/scenes/MainView.tscn")
    if Input.is_action_just_pressed("fscrn"):
        OS.window_fullscreen = !OS.window_fullscreen
        if not OS.window_fullscreen:
            OS.set_window_size(Vector2(840,480))
func shoot(pos, off, dir: int, player):
    var bullet = Bullet.instance().init(Vector2(dir,0), pos+off) 
    if player:
        bullet.add_to_group("PlayerBullet")
    else:
        bullet.add_to_group("EnemyBullet")
    MainNode.add_child(bullet)

func distance(pos1, pos2):
    return sqrt(
        pow(pos1.x - pos2.x,2) + 
        pow(pos1.y - pos2.y,2)
    )

func toggle_platforms():
    var children = white_platforms.get_children()
    for child in children:
        if child.is_in_group("Platform"):
            child.visible = !child.visible
            child.collider.set_deferred("disabled", !child.visible)
