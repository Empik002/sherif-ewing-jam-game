extends KinematicBody2D

onready var path_follow = get_parent().get_parent()
onready var anim = get_node("AnimatedSprite")

const SPEED = 20
var VISION = 25
var STOPDISTANCE = 25
const OFFSET_L = Vector2(-5,-1)
const OFFSET_R = Vector2(5,1)
var SHOT_TIME = 1.5

var time_to_shot = SHOT_TIME
var distance = 100

var old_pos = Vector2(0,0)

func _process(delta):
    time_to_shot = max(0, time_to_shot - delta)
    if time_to_shot <= 0 and distance <= VISION:
        shoot()
        time_to_shot = SHOT_TIME


func _physics_process(delta):
    var player_pos = Global.Player.get_global_position()
    var this_pos = get_global_position()
    distance = Global.distance(player_pos, this_pos)
    
    anim.flip_h = dir(player_pos, this_pos)
    anim.playing = distance > STOPDISTANCE
    if distance > STOPDISTANCE:
        var new_offset = path_follow.get_offset() + SPEED * delta
        anim.flip_h = not dir(old_pos, this_pos)
        path_follow.set_offset(new_offset)
    
    old_pos = this_pos
    
func dir(pos1, pos2):
    var diff = pos1.x - pos2.x
    return sign(diff) == 1

func shoot():
    var ofst
    var direction
    
    if not anim.flip_h:
        ofst = OFFSET_L
        direction = -1
    else:
        ofst = OFFSET_R
        direction = 1
    
    Global.shoot(get_global_position(), ofst, direction, false)
