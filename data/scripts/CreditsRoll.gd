extends Sprite

var speed = 25
var vel = 0
func _process(delta):
    var pos = get_global_position()
    get_input()
    pos.y -= vel * speed * delta
    pos.y = max(-272,min(0, pos.y))
    set_global_position(pos)

func get_input():
    vel = 0
    if Input.is_action_pressed("left"):
        vel = -1
    if Input.is_action_pressed("right"):
        vel = 1 
