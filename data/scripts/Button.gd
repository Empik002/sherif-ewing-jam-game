extends Node2D
onready var anim = get_node("Sprite")


func _process(delta):
    if Global.SwitchColors:
        anim.frame = 1
    else:
        anim.frame = 0


func body_entered(body):
    if body == Global.Player:
        Global.SwitchColors = !Global.SwitchColors
        Global.toggle_platforms()
