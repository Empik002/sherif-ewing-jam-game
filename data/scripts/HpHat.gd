extends Node2D

func body_entered(body):
    if body == Global.Player:
        Global.Player.heal()
        queue_free()
