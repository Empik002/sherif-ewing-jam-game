extends AnimatedSprite

var animation_ended = false

func _ready():
    frame = 0
    playing = true

func _process(delta):
    if animation_ended:
        if Input.is_action_just_pressed("shoot"):
            get_tree().change_scene("res://data/scenes/GameView.tscn")
func anim_ended():
    animation_ended = true
