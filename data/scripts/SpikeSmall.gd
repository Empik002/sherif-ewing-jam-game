tool
extends Node2D

onready var collider = get_node("StaticBody2D/CollisionPolygon2D")
onready var sprite = get_node("Sprite")
export var SpikeNumber: int = 1

const W = 4
const H = 3

func _ready():
    var sprite_size = Vector2(SpikeNumber*W - 1, H)
    sprite.region_rect= Rect2(Vector2(1,4), sprite_size)
    
    var polygon = [
        Vector2(0,0),
        Vector2(W*SpikeNumber - 1, 0),
        Vector2(W*SpikeNumber - 1, H),
        Vector2(0, H)
    ]
    collider.polygon = PoolVector2Array(polygon)

func _process(_delta):
    if Engine.editor_hint:
        var collider = get_node("StaticBody2D/CollisionPolygon2D")
        var sprite = get_node("Sprite")
        var sprite_size = Vector2(SpikeNumber*W - 1, H)
        sprite.region_rect= Rect2(Vector2(1,4), sprite_size)
        
        var polygon = [
            Vector2(0,0),
            Vector2(W*SpikeNumber - 1, 0),
            Vector2(W*SpikeNumber - 1, H),
            Vector2(0, H)
        ]
        collider.polygon = PoolVector2Array(polygon)
