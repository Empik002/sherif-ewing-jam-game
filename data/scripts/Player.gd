extends KinematicBody2D

onready var anim = get_node("Player_Sprite")
onready var HUD = get_node("HUD")
onready var collider = get_node("CollisionShape2D")

var jump_sound = preload("res://data/sfx/jump.wav")
var sfx:AudioStream = preload("res://data/sfx/sfx01.wav")
var shoot_sound = preload("res://data/sfx/shoot.wav")
var hurt_sound: AudioStream = preload("res://data/sfx/hurt.wav")
var Hat = preload("res://data/scenes/Hat.tscn")
var Bullet = preload("res://data/scenes/Bullet.tscn")

const GRAVITY = 3
const JUMP = 85
const MAXHP = 5
const MAX_INVINCIBLE_TIME = .75
const MAX_FLICKER_TIME = 0.1
const KNOCKBACK = Vector2(25,-55)
const MAXSPEED = Vector2(25,85)
const OFFSET_R = Vector2(5,0)
const OFFSET_L = Vector2(-7,0)
const SPEED = 25
const MAX_SHOOT_TIME = 0.5
const DEATHSCENES = {
    "Default":"res://data/scenes/DefaultDeath.tscn",
    "Cactus":"res://data/scenes/CactusDeath.tscn",
    "Fire":"res://data/scenes/BurnDeath.tscn",
    "Spike":"res://data/scenes/SpikeDeath.tscn",
    "Explode":"res://data/scenes/ExplodeDeath.tscn",
    "EnemyBullet": "res://data/scenes/ShootDeath.tscn"
    }

var velocity = Vector2.ZERO
var health = [] # max health = 3
var invincible_time = 0
var move_limit = 0.1
var flicker_time = MAX_FLICKER_TIME
var shoot_time = MAX_SHOOT_TIME

var fps = 0
var fps_time = 0
func _ready() -> void:
    Global.Player = self
    for _i in range(3):
        heal()

func _process(delta):
    invincible_time = max(0, invincible_time - delta)

func _physics_process(delta: float) -> void:
    fps_time += delta
    fps += 1
    if fps_time >= 1:
        fps_time = 0
        #print(fps)
        fps = 0
    
    if invincible_time != 0:
        flicker_time -= delta
        if flicker_time <= 0:
            visible = !visible
            flicker_time = MAX_FLICKER_TIME
    else:
        visible = true
        flicker_time = MAX_FLICKER_TIME
        
    shoot_time = max(0,shoot_time - delta)
    move_limit = delta * 10
    velocity.y += GRAVITY
    velocity = move_and_slide(velocity,Vector2.UP)
    get_input()
    # not very nice , with those <move_limit>, could break 
    if velocity.x == 0 and abs(velocity.y) < move_limit:
        anim.animation = "walk"
        anim.frame = 1
        anim.playing = false
    elif velocity.y < 0:
        anim.animation = "jump"
    elif velocity.y > move_limit:
        anim.animation = "fall"
    else:
        anim.animation = "walk"
        anim.playing = true
    if velocity.x != 0:
        anim.flip_h = velocity.x < 0
        #flip_hats(velocity.x < 0)
    
    var collisions = get_slide_count()
    for i in range(0,collisions):
        var collision = get_slide_collision(i)
        if collision.get_collider().is_in_group("Hurts"):
            hurt(collision.get_collider())  
            var old_vel = Vector2(velocity.x, velocity.y)
            velocity = move_and_slide(velocity)
            
            # bounces you the other way if you get stuck 
            # (so you dont get stuck on some cactus and what not)
            if abs(velocity.x) < move_limit:
                velocity.x = sign(old_vel.x) * KNOCKBACK.x
            return
        
func get_input():
    if invincible_time > MAX_INVINCIBLE_TIME / 2 and velocity != Vector2.ZERO:
        return
    velocity.x = 0
    if Input.is_action_pressed("left"):
        velocity.x -= 1
    if Input.is_action_pressed("right"):
        velocity.x += 1
    if Input.is_action_pressed("jump") and is_on_floor():
        if velocity.y == 0 and is_on_floor():
            velocity.y -= JUMP
            AudioManager.play_sfx(jump_sound)
    if Input.is_action_just_pressed("shoot"):
        if shoot_time <= 0:
            shoot()
            shoot_time = MAX_SHOOT_TIME
    velocity.x = velocity.x * SPEED

func hurt(collider): # make him flicker
    if invincible_time != 0:
        return
    
    AudioManager.play_sfx(hurt_sound)   
    invincible_time = MAX_INVINCIBLE_TIME
    var x = health.pop_back()
    HUD.remove_child(x)
    x.free() # removes hat from existence
    knockback()
    if len(health) == 0:
        die(collider)

func hurt_bullet(body): # OR DYNAMITE OR FIRE
    if body.is_in_group("EnemyBullet"): 
        hurt(body)
        body.remove_and_skip()

func hurt_boom():
    var foo_collider = Node2D.new()
    foo_collider.add_to_group("Explode")
    hurt(foo_collider)

func hurt_fire():
    var foo_collider = Node2D.new()
    foo_collider.add_to_group("Fire")
    hurt(foo_collider)
 
func die(collider):
    var scene = DEATHSCENES["Default"]
    for group in DEATHSCENES:
        if collider.is_in_group(group):
            scene = DEATHSCENES[group]
            break
    get_tree().change_scene(scene)
    
    
func heal():
    if len(health) < MAXHP:
        var currentHat = Hat.instance()
        currentHat.position.y = 4
        currentHat.position.x = 4 + 5*len(health)
        health.append(currentHat)
        HUD.add_child(currentHat)


func knockback():
    if anim.flip_h:
        velocity = KNOCKBACK
    else:
        velocity = Vector2(KNOCKBACK.x * (-1),KNOCKBACK.y)


func shoot():
    var ofst
    var dir
    
    if anim.flip_h:
        ofst = OFFSET_L
        dir = -1
    else:
        ofst = OFFSET_R
        dir = 1
    
    Global.shoot(get_global_position(), ofst, dir, true)
    AudioManager.play_sfx(shoot_sound)
    
