extends KinematicBody2D

onready var Explosion = preload("res://data/scenes/Explosion.tscn")
var velocity = Vector2.ZERO
const GRAVITY = 3
const BLAST_RADIUS = 11

func _physics_process(delta):
    velocity.y += GRAVITY
    velocity = move_and_slide(velocity)
    if get_slide_count() > 0:
        velocity.x = 0
        explode()
        
        
func init(vel):
    velocity = vel
    return self
    

func explode():
    var this_pos = get_global_position()
    var player_pos = Global.Player.get_global_position()
    if Global.distance(this_pos, player_pos) <= BLAST_RADIUS:
        Global.Player.hurt_boom()
    var new_explosion = Explosion.instance().init(this_pos)
    Global.MainNode.add_child(new_explosion)
    queue_free()
