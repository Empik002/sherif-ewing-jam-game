extends Node2D

var hp = 3
var die_sound = preload("res://data/sfx/die.wav")
func object_entered(body): # play sound
    if body.is_in_group("PlayerBullet"):
        hp -= 1
        body.queue_free()
        AudioManager.play_sfx(die_sound)
        if hp > 0:
            return
        get_parent().remove_child(self)
        queue_free()
        



