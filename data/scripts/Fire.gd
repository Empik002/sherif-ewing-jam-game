extends KinematicBody2D

var duration = 4
var velocity = Vector2.ZERO
const GRAVITY = 3

func _physics_process(delta):
    velocity.x = 0
    velocity.y += GRAVITY
    velocity = move_and_slide(velocity)
    
func _process(delta):
    duration -= delta
    if duration <= 0:
        queue_free()


func body_entered(body):
    if body == Global.Player:
        Global.Player.hurt_fire()

func init(pos):
    set_global_position(pos)
    return self
