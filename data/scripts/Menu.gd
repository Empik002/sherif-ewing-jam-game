extends Node2D

onready var anim = get_node("Background")
onready var play_b = get_node("PlayButton")
onready var quit_b = get_node("QuitButton")
onready var cred_b = get_node("CreditsButton")

var selected = 0
onready var buttons = [play_b, quit_b, cred_b]

func _ready():
    anim.playing = true


func _process(delta):
    if Input.is_action_just_pressed("ui_down"):
        selected = limit(selected + 1)
        select()
    elif Input.is_action_just_pressed("ui_up"):
        selected = limit(selected - 1)
        select()
    elif Input.is_action_just_pressed("shoot"):
        exec()
        
        
func select():
    for i in range(len(buttons)):
        if i == selected:
            buttons[i].frame = 1
        else:
            buttons[i].frame = 0 


func limit(number):
    return max(min(2,number),0)


func exec() :
    if selected == 0:
        get_tree().change_scene("res://data/scenes/Controls.tscn")
    elif selected == 1:
        get_tree().quit()  
    elif selected == 2:
        get_tree().change_scene("res://data/scenes/CreditsView.tscn")
