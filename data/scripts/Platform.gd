tool
extends Node2D

onready var collider = get_node("SolidPlatform/StaticBody/CollisionPolygon2D")
export var width: int = 10
export var height: int = 2
export var darkColor: bool = true
export var isVisible: bool = true

var old_width = 10
var old_height = 2

func _ready():
    visible = isVisible
    change_size()

func _process(_delta):
    if Engine.editor_hint:
        position.x = int(position.x)
        position.y = int(position.y)
        visible = isVisible
        if old_width != width or old_height != height:
            change_size()

func change_size():
    var color_rect = get_node("SolidPlatform")
    var collider = get_node("SolidPlatform/StaticBody/CollisionPolygon2D")
    
    var polygon = [
        Vector2(0,0),
        Vector2(width, 0),
        Vector2(width, height),
        Vector2(0, height)
       ]
    
    
    color_rect.rect_size = Vector2(width, height)
    collider.polygon = PoolVector2Array(polygon)
