extends KinematicBody2D

var direction = Vector2.ZERO
var starting_point = Vector2.ZERO

const SPEED = 35
const RANGE = 45

func _ready():
    pass

func init(dir, pos):
    direction = dir
    set_global_position(pos)
    starting_point = pos
    return self

func _physics_process(delta):
    move_and_slide(direction * SPEED)
    
    if (
        get_slide_count() > 0 or 
        Global.distance(get_global_position(), starting_point) > RANGE
        ):
        queue_free()
