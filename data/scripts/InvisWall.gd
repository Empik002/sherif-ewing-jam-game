tool
extends Node2D

export var half_width = 10
export var half_height = 10

func _ready():
    var collider = get_node("InvisWall/CollisionShape2D")
    collider.shape.set_extents(Vector2(half_width, half_height))

func _process(_delta):
    if Engine.editor_hint:
        var collider = get_node("InvisWall/CollisionShape2D")
        collider.shape.set_extents(Vector2(half_width, half_height))
