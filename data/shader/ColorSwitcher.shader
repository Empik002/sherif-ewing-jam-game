shader_type canvas_item;

uniform vec4 dark : hint_color;
uniform vec4 light : hint_color;
uniform bool run = false;

void fragment() {    
    vec4 col = texture(TEXTURE, UV);
    if (run){
        if (round(col) == round(dark)){
            col = light
        }
        else {
            col = dark    
        }
    }
    COLOR = col;
}