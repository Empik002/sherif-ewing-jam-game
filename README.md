## Sheriff Ewving - a game jam game
This is a short platformer game made in GODOT game engine in a week for Nokia 3310 Game
Jam (2021). A jam where you create a game with some of the limitations of nokia 3310
phone like only 80x40 pixels!

We finished **#38 out of 313** submissions!

# Authors
- Art: © Dorota Zimová 2021
- Code: Martin Pavelka

# Building
To build this project you will require the GODOT game engine. <br>
Avalible at <a href="https://godotengine.org/"> their website </a> or on Steam

Or you can **download builds** from my <a href="https://empik.itch.io/sheriff-ewing"> itch.io page </a>

<br><br><br>
<img src="https://img.itch.zone/aW1hZ2UvOTEyNjEyLzUxNTkxNTkucG5n/original/T2sjoG.png">
